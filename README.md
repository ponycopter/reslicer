# README #

Reslicer is an ImageJ macro which can be used to...

* import microCT a scan volume (image sequence/stack), 
* straighten sample orientation using the reslice function in three axes, and
* export median Z projections for improved image quality


The included sample stack allows you to test the Macro on a small dataset. The median projection don't show a lot of difference because it's Synchrotron footage, so the image quality is already relatively good. 
Median projections can notably improve image quality (and facilitate image analysis) in volumes with higher image noise. For an example, see Fig. S1 in Nolf et al., 2017:

![](https://choatlab.files.wordpress.com/2017/11/nolfetal2017-figs1.png)
Image quality comparison between a single 2D cross section (left) and a median Z-projection across 100 μm (median of eleven 2D cross sections; right). Scale bar = 1 mm.



### Literature ###

Nolf M, Lopez R, Peters JMR, Flavel RJ, Koloadin L, Young IM, Choat B (2017) Visualisation of xylem embolism by X-ray microtomography: a direct test against hydraulic measurements. New Phytologist doi: 10.1111/nph.14462
