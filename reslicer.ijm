//====== Reslicer
// ImageJ Macro to import microCT a scan volume (image sequence/stack), 
// straighten sample orientation using the reslice function,
// and export median Z projections for improved image quality (better signal-to-noise-ratio)
// by Markus Nolf, https://bitbucket.org/ponycopter/reslicer

Dialog.create("Choose actions");
Dialog.addCheckbox("Import stack (deactivate if your stack is already open in ImageJ)",true);
Dialog.addCheckbox("  + Partial stack import",true);
Dialog.addCheckbox("Reslice / straighten stack",true);
Dialog.addCheckbox("  + Save resliced stack",true);
Dialog.addCheckbox("Save median projection",true);
Dialog.addHelp("https://bitbucket.org/ponycopter/reslicer");

Dialog.show();


doimport = Dialog.getCheckbox();
dopartialimport = Dialog.getCheckbox();
doreslice = Dialog.getCheckbox();
dosavereslicedstack = Dialog.getCheckbox();		
dosavemedian = Dialog.getCheckbox();		

if(doimport==1) {
  dir = getDirectory("Choose Source Directory");	
}
if(dosavereslicedstack+dosavemedian>0) {
  outputdir = getDirectory("Choose Output Directory");	
}

if(doimport==1) {
  
  newlist = getFileList(dir);
  
  nfiles = newlist.length;
  importxslices = nfiles;
  centreslice = floor(nfiles/2);
  startslice=floor(centreslice-importxslices/2); 
  
  if(dopartialimport==1) {
    
    Dialog.create("Partial import");
    Dialog.addMessage("Partial import");
    Dialog.addNumber("Import how many slices?", nfiles); 
    Dialog.addNumber("Centre slice?", centreslice); 
    Dialog.show();
    
    importxslices = Dialog.getNumber();
    centreslice = Dialog.getNumber();
    
    startslice=floor(centreslice-importxslices/2);
    if(startslice<0) { startslice = 0; }
  }
  if(startslice<0) { startslice = 0; }
  endslice = startslice+importxslices;
  
  
  z=0;
  newtemplatefile = newlist[z];
  while(!endsWith(newtemplatefile,".jpg") & !endsWith(newtemplatefile,".tif") & !endsWith(newtemplatefile,".png")) {
    z=z+1;
    newtemplatefile = newlist[z];
  }
  loadsequence = dir+newtemplatefile;
  
  print("Importing slices "+startslice+" to "+endslice+".");
  run("Image Sequence...", "open=[loadsequence] increment=1 number=[importxslices] starting=[startslice] scale=100 sort"); 
  
  
  setTool("rectangle");
  waitForUser("Select crop area.");
  run("Crop");
}
if(dosavereslicedstack==1) {
  savenewstack = 	outputdir + "resliced-stack";
  File.makeDirectory(savenewstack);
}
if(dosavemedian==1) {
  savemedianto = 	outputdir + "resliced-medians";
  File.makeDirectory(savemedianto);
}

if(doreslice==1) {
  
  var slicecount;
  var autoslicecount;
  samplename=getTitle();
  
  run("Reslice [/]...","start=Top avoid");
  run("Out [-]");
  tmpname=getTitle();
  
  
  setTool("line");
  waitForUser("Step 1: \nSelect sample midpoint / widest part of the sample, \nDraw a line along a sample vessel from bottom to top*, \nThen move the line to the left edge of the image.\n \n(*You may need to follow a vessel through neighbouring slices.)");
  
  imagewidth=getWidth();
  run("Reslice [/]...","slice_count="+imagewidth*1.5+" avoid");
  
  tmpname2=getTitle();
  selectWindow(tmpname);
  close();
  selectWindow(tmpname2);
  run("Out [-]");
  
  
  waitForUser("Step 2: \nSelect sample midpoint / widest part of the sample, \nDraw a line along a sample vessel from left to right, \nThen move the line to the top edge of the image.");
  imageheight=getHeight();
  run("Reslice [/]...","slice_count="+imageheight*1.5+" avoid");
  tmpname3=getTitle();
  selectWindow(tmpname2);
  close();
  selectWindow(tmpname3);
  run("Out [-]");
  
  run("Reslice [/]...","start=Right avoid");
  tmpname4=getTitle();
  selectWindow(tmpname3);
  close();
  selectWindow(tmpname4);
  
  newsamplename = samplename+"-resliced-0";
  rename(newsamplename);
  setTool("rectangle");
  run("Out [-]");
  
  
  waitForUser("Select re-crop area (or select all).");
  run("Crop");	
  
  if(dosavereslicedstack==1) {
    print("Saving resliced stack to: \n"+savenewstack);
    run("Image Sequence... ","format=TIFF save=["+savenewstack+"/"+newsamplename+".tif]");	
  }
}


if(dosavemedian==1) {
  //export medians. uneven numbers only for true median values.
  x = 1;
  waitForUser("Find a good centre slice for Median projections.");	
  
  origwindow = getTitle();
  zslice = getSliceNumber();
  
  print("Saving median projections to: \n"+savemedianto);
  
  function exportmedian(howmany,origwindow) {
    selectWindow(origwindow); 
    if(howmany>0) {
      run("Z Project...", "start="+zslice-howmany+" stop="+zslice+howmany+" projection=Median");
      newfilename = origwindow+"-zproj-median-s"+zslice+"-plusminus"+howmany;
    }
    if(howmany==0) {
      run("Make Substack...", "  slices="+zslice);
      newfilename = origwindow+"-zproj-median-s"+zslice+"-plusminus"+howmany+"-single";
    }
    rename(newfilename);
    savefile = savemedianto+"/"+newfilename+".tif"; 
    
    saveAs("Tiff",savefile); //mn
  }	
  exportmedian(10,origwindow);
  exportmedian(5,origwindow);
  exportmedian(2,origwindow);
  exportmedian(0,origwindow);
}

print("All done. \n ");

Dialog.create("All done");
Dialog.addMessage("All done.");
Dialog.addCheckbox("Close all open windows?",true);
Dialog.show();

docloseall = Dialog.getCheckbox();

if(docloseall==true) {
  run("Close All");
}
